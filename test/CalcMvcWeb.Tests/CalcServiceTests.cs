﻿using CalcMvcWeb.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalcMvcWeb.Tests
{

    [TestClass]
    public class CalcServiceTests
    {
        [TestMethod]
        public void TestAddNumbers()
        {
            int x = 3; int y = 2; int expectedResult = 5;
            // 1. Arrange
            var cs = new CalcService();

            // 2. Act 
            var result = cs.AddNumbers(x, y);

            // 3. Assert 
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestSubtractNumbers()
        {
            int x = 3; int y = 2; int expectedResult = 1;
            var cs = new CalcService();
            var result = cs.SubtractNumbers(x, y);
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestMultiplyNumbers()
        {
            int x = 3; int y = 2; int expectedResult = 6;
            var cs = new CalcService();
            var actualResult = cs.MultiplyNumbers(x, y);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestEvenNumberWithBoolCompare()
        {
            int x = 4; bool expectedResult = true;
            var cs = new CalcService();
            var actualResult = cs.IsEven(x);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void TestEvenNumberForTrueResult()
        {
            int x = 6;
            var cs = new CalcService();
            var actualResult = cs.IsEven(x);
            Assert.IsTrue(actualResult);
        }

        [TestMethod]
        public void TestEvenOrOdd()
        {
            int x = 4; bool isEven = true;
            var cs = new CalcService();
            var actualResult = cs.IsEvenOrOdd(x);
            Assert.AreEqual(isEven, actualResult);
        }
    }
}
